<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $fillable=['title', 'body','user_id'];

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function likedByCurrentUser(){
        $userId=auth()->id();

        //like boolean
        $like = $this->likes->first(function($v) use ($userId){
            //$v is a reference to the single like
            return $v->user_id == $userId;
        });

        //if the user has liked a post
        if($like){
           return true;
       }
         return false;

   }
}
