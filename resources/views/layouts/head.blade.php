<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Free Social Media Website">
  <meta name="keywords" content="share, like, sell, market, users, brand, comment, post">
  <meta name="author" content="Kneuma Design">
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="/css/bootstrap.min.css" rel="stylesheet">
<link href="/css/nav.css" rel="stylesheet">
<link href="/css/main.css" rel="stylesheet">
<link href="/css/comments.css" rel="stylesheet">
<link href="/css/scrollability.css" rel="stylesheet">
