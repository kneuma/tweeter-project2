

<div class="fixed-nav">
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a href="#menu-toggle" class="btn btn-dark float-left" id="menu-toggle">
        <div class="hambuga"></div>
        <div class="hambuga"></div>
        <div class="hambuga"></div>
    </a>
  <!-- Brand -->


  <a class="navbar-brand" href="/">
      <img src="/imgs/cluster-logo.png" width="50px" height="50px">
      <h1 class="float-left">Cluster</h1>

  </a>


  <!-- Links -->
   @if (Auth::user())
      <div class="desktop-nav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="/feed">Feed</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/listusers">Follow</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="/profile">Profile</a>
            </li>

            <!-- Dropdown -->
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                Settings
              </a>
              <div class="dropdown-menu">

                <h3 style="text-align:center;">{{ Auth::user()->name }}</h3>
                <a class="dropdown-item" href="/auth/logout">Logout</a>
                <a class="dropdown-item" href="/profile/edit">Edit Profile</a>
                <a class="dropdown-item" href="#">Delete Account</a>
              </div>
            </li>
          </ul>

    </div>
    @endif
    @if(session('message'))
        <p class="alert alert-success">
            {{ session('message')}}
        </p>
    @endif

    @if(session('errorMessage'))
        <p class="alert alert-danger">
            {{session('errorMessage')}}
        </p>

    @endif
</div>
</nav>
</div>


<div id="wrapper">
  <!-- Sidebar -->
  <div id="sidebar-wrapper">
      <ul class="sidebar-nav">
          @if (Auth::user())
          <div class="mobile-nav">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="/feed">Feed</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/follow">Follow</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="/profile">Profile</a>
                </li>

                <!-- Dropdown -->
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                    Settings
                  </a>
                  <div class="dropdown-menu">

                    <a class="dropdown-item" href="/auth/logout">Logout</a>
                    <a class="dropdown-item" href="/profile/edit">Edit Profile</a>
                    <a class="dropdown-item" href="#">Delete Account</a>
                  </div>

                </li>
              </ul>
        </div><!---End of mobile-nav--->
         @endif

        <li>
            <a href="#">Trending</a>
        </li>
        <li>
            <a href="#">Groups</a>
        </li>
        <li>
            <a href="#">Privacy</a>
        </li>
        <li>
            <a href="#">Contact</a>
        </li>
         @if (Auth::guest())
        <a href="/auth/login"><button class="btn btn-light btn-lg btn-block">Login</button></a>
        <a href="#register"><button class="btn btn-dark btn-lg btn-block">Register</button></a>
        @endif
      </ul>

  </div>
</div><!-- /#sidebar-wrapper -->
