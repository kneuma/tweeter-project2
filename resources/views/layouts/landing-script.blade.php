    <script src="js/jquery.min.js"></script>
    <script src="js/particles.js"></script>
    <script src="js/particlesapp.js"></script>
    <script src="js/parallax.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="js/jScrollability.js"></script>
    <script src="js/scroll.js"></script>

    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>
