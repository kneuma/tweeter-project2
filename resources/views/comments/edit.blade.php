<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <title>Cluster Edit Comment</title>

        @include('layouts/head')

    </head>
    <body>
    @include('layouts/nav')

    <div class="page-container2">
        <div class="container">
            <br />
            <h1>Edit Your Comment</h1>
            <br />


                <form action="/tweets/{{ $comment->id }}/comments" method="POST">
                <input type="hidden" name="_method" value="PUT"/>


                    {{ csrf_field()}}

                <div class="form-group">

                    <textarea name="body" placeholder="Enter your comment" class="form-control">{{ $comment->body}}</textarea>
                </div>

                <div class="form-group">

                <button class="btn btn-primary" type="submit">Edit Comment</button>

                </div>

                </form>

                <div class="form-group">
                    <form action="/tweets/{{ $comment->id }}/comments" method ="POST">
                    <input type="hidden" name ="_method" value="DELETE" />
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">Delete Comment</button>

                </div>
            </div>
        </div>
        @include('layouts/script')
    </body>
</html>
