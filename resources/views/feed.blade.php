<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <title>Cluster Edit Profile</title>

        @include('layouts/head')

    </head>
<body>
    @include('layouts/nav')

    <div class="container">

        <br />
        <h1>Create a Tweet</h1>
        <br />
            <form action="/tweets" method="POST">
                {{csrf_field()}}


                <div class="form-group">
                    <label>Body</label>
                    <textarea class="form-control" name="body" placeholder="Write A Post"/></textarea>

                </div>

                <div class="form-group">

                    <button class="btn btn-primary" type="submit">Save Tweet</button>

                </div>

            </form>

        </div>

        <div class="feed-box">
            <h2>Tweeter Feed</h2>
            @include('tweets/index')
        </div>

        @include('layouts/script')
    </body>
</html>
