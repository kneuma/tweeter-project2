<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <title>Cluster: {{ $user->username }}'s' Profile</title>
        @include('layouts/head')


</head>

    <body>

        @include('layouts/nav')

        @include('profile/profile-layout')

        <div class="page-header">
            <h2>{{ $user->username }}'s Tweets</h2>
        </div>

        <div class="container">

            <div class="feed-box">
                <div id="app">
                    @include('tweets/index')
                </div>
            </div>
        </div>
        @include('layouts/script')
    </body>
</html>
