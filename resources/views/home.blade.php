<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="description" content="Free Social Media Website">
        <meta name="keywords" content="share, like, sell, market, users, brand, comment, post">
        <meta name="author" content="Kneuma Design">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cluster Login/Register Now</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/nav.css" rel="stylesheet">
        <link href="css/mp.css" rel="stylesheet">
        <link href="css/scrollability.css" rel="stylesheet">

</head>


<body>

@include('layouts/nav')


  <div class="parallax" data-parallax="scroll" data-z-index="-100" data-image-src="imgs/milky-way.jpg">

      <div id="particles-js">

      </div><!---End of particles--->

  </div><!---End of parallax--->



  <div class="home-container">
      <div class="major-bg1">

                      <div class="home-text">

                          <h2>Join Cluster</h2>

                              <p>
                                  A more creative outlet for groups, artists,
                                  & businesses to explore, design, and brand
                                  their social media experience.
                              </p>
                              <a href="/auth/login"><button class="btn btn-primary btn-lg">Login</button></a>

                              <a href="#register"><button class="btn btn-primary btn-lg">Register</button></a>


                      </div><!---End of home-text--->

          </div><!---End of major-bg1--->


      </div><!---End of home container--->



  <div class="major-bg2">
      <div class="section section-1">
          <span class="slide-in-text">See the Latest <br />from your friends<br /> and followers </span>
          <img src="imgs/feed.png" alt="post-comment-feed"/>
      </div>
  </div><!---End of major-bg2--->



  <div class="parallax" data-parallax="scroll" data-z-index="-100" data-image-src="imgs/geo-bg.jpg">
      <div class="home-title right">
          <h2>Cluster Users Are:</h2>
      </div><!---End of home-title--->

        <div class="home-text2">
            <div class="row">
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="imgs/social2.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Individuals</h5>
                            <p class="card-text">
                                Share all of your favorite moments with friends
                                and others, browse Cluster for trending topics and news,
                                join and subscribe to exclusive content and groups and,
                                meet other's through shared interests and likes.
                            </p>

                        </div><!---End of Card body--->
                    </div><!---End of card--->
                </div>
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="imgs/social1.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Artists & Creatives</h5>
                            <p class="card-text">
                                Use Cluster's quick filters & editing tools to
                                astonish your viewers with your art and photography

                            </p>

                        </div><!---End of Card body--->
                    </div><!---End of card--->
                </div><!---End of col--->
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="imgs/business.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">Business</h5>
                            <p class="card-text">
                                Use Cluster's personal and social interface to reach
                                your audience, customize your page to match your Brand
                                 with Cluster's unique page editing tools,
                                and even sell your products right from Cluster
                            </p>

                        </div><!---End of Card body--->
                    </div><!---End of card--->
                </div>
                <div class="col">
                    <div class="card" style="width: 18rem;">
                        <img class="card-img-top" src="imgs/blogger.jpg" alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title">News & Bloggers</h5>
                            <p class="card-text">
                                Share your stories and blogs right in Cluster,
                                offer sellable or subscribable content and
                                use our visual editing tools to astonish your readers.

                            </p>

                        </div><!---End of Card body--->
                    </div><!---End of card--->
                </div><!---End of col--->
            </div><!---End of row--->
        </div><!---End of home-txt2--->
    </div><!---End of Parallax--->

<div class="parallax" data-parallax="scroll" data-z-index="-100" data-image-src="imgs/bg-graphic.jpg">

        <div class="home-title left">
            <h2>At Cluster You Can:</h2>
        </div><!---End of home-title--->

        <div class="home-text3">
            <div class="row">
                <div  class="col">

                        <h3><img src="/imgs/share.png" alt="friends-icon"/>Connect & Share</h3>
                        <p>
                            Exchange posts, comments, gifs, stories, and events  with anyone or everyone.
                            Cluster was built to make socializing with your current friends or making
                            new friends easier and more fun than ever before.
                        </p>

                </div>
                <div  class="col">
                    <h3><img src="/imgs/lock.png" alt="lock-icon"/> Keep Your Info Private & Secure </h3>
                    <p>
                        Feel free to share anything with Cluster's straight-forward & customizable privacy settings.
                        Your information is safe with us, and isn't sold or searchable outside of Cluster.
                    </p>
                </div>
            </div>

            <div class="row">
                <div  class="col">

                        <h3><img src="/imgs/file.png" alt="file-share-icon"/>Collaborate With Peers & Coworkers</h3>
                        <p>
                            Use our cloud file sharing and private messaging to instantly
                            collaborate as a team and get the job done together.
                        </p>

                </div>
                <div  class="col">
                    <h3><img src="/imgs/money.png" alt="money-icon"/>Sell Your Products & Services</h3>
                    <p>
                        Create and brand your online store and use Cluster's easy to manage
                        ad campaign interface. Reaching out to your customers has never been easier.
                    </p>
                </div>
            </div>

            <div class="row">
                <div  class="col">

                    <h3><img src="/imgs/monitor.png" alt="monitor-icon"/>Join or Offer Subscription Content</h3>
                    <p>
                        Cluster wants users and business's to offer high quality content.
                        By offering the option of having subscription content, you can
                        really put in the difference when you have the opportunity to earn from Your
                        content. <a href="#">See Cluster's subscription content advice for more details.</a>

                </div>
                <div  class="col">
                    <h3><img src="/imgs/earth.png" alt="earth-icon"/> Politics, Organizations and Current Events </h3>
                    <p>
                        Be involved with what's happening in the world, learn, join, help, or even donate.
                        Cluster maintains an easy access fact checker that you help maintain and certifies
                        organizations and News channels so that you can rely on what you see on Cluster.
                        <a href="#">See how you can help keep information real</a>
                    </p>
                </div>
            </div>
        </div><!---End of home-text2--->

</div>
<div class="major-bg3">

    <div class="home-title center">
        <a name="register"><h2>Join Cluster Today</h2></a>
    </div>



    @include('auth/register/create')
    <div class="clouds-desktop">
        <img src="imgs/cloud-footer.gif">
    </div>

    <div class="clouds-mobile">
        <img src="imgs/cloud-footer.gif">
    </div>

</div>




@include('layouts/landing-script')




</body>
</html>
