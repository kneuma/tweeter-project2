<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <title>Cluster Follow</title>
        @include('layouts/head')


</head>
@include('layouts/nav')

<body>
  <body class="bg-light">

    <main role="main" class="container">
        <br/>
        <h6 class="border-bottom border-gray pb-2 mb-0">{{ $pagetitle }}</h6>

        @foreach ($users as $user)
        <a href="/profile/{{ $user->id }}">
         <div class="my-3 p-3 bg-white rounded box-shadow">

        <div class="media text-muted pt-3">

              <img src="/uploads/avatars/{{ $user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">

          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark"><h5>{{ $user->username }}</h5></strong>
              @if($user->amFollowing())
              <a class="btn btn-primary btn-sm btn-danger" href="{{ route('user.unfollow', $user->id) }}" role="button">Unfollow</a>
              @else
              <a class="btn btn-primary btn-sm" href="{{ route('user.follow', $user->id) }}" role="button">Follow </a>

              @endif
            </div>
            <span class="d-block">Location: {{ $user->location }}</span>
            <span class="d-block"><br /> {{ $user->bio }}</span>
          </div>
        </div>

        <small class="d-block text-right mt-3">

        </small>
      </div>
    </a>

    @endforeach
    </main>
    @include('layouts/script')
  </body>
</html>
