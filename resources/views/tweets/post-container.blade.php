
<div id="app">
    <div class="feed-panel">
        <div class="row">
            <div class="col-4">

                <a href="/profile/{{ $tweet->user->id }}"><strong class="text-success">{{ $tweet->user->name }}</strong></a>

            </div><!---End of col sm--->

            <div class="col-8">

                <small class="text-muted">{{ $tweet->created_at }} </small>

                <div class="dropdown float-right" >
                    <button type="button" class="btn btn-outline-light dropdown-toggle" data-toggle="dropdown">
                        <span class="dot"></span>
                        <span class="dot"></span>
                        <span class="dot"></span>
                    </button>


                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">

                        <button class="dropdown-item" type="button"> Edit Post</button>

                        <button class="dropdown-item" type="button">Embed Post</button>
                        <button class="dropdown-item" type="button">Report</button>
                    </div><!---End of dropdown-menu--->

                </div><!---End of dropdown--->
            </div><!---End of col sm--->

        </div><!---End of row--->
    </div><!---End of feed-panel--->

    <div class="post-container">
        <div class="row">
            <div class="col-4">

                <a href="/profile/{{ $tweet->user->id }}">
                    <img src="/uploads/avatars/{{ $tweet->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                </a>

            <div class="likes">

                <like-button
                    like-count="{{ count($tweet->likes) }}"
                    has-liked="{{ $tweet->likedByCurrentUser() }}"
                    tweet-id="{{ $tweet->id }}">

                </like-button>

            </div><!---End of likes--->
        </div><!---End of col-sm--->


        <div class="col-8">
        <div class="post">

            <blockquote class="blockquote mb-0">

                <p>{{ $tweet->body }}</p>

            </blockquote>

        </div><!---End of post--->
    </div><!---End of col-8--->
    </div><!---End of row--->
    </div><!---End of post container--->

    <div class="feed-panel">

        <button type="button" class="btn btn-outline-light" data-toggle="collapse" data-target="#comments">See Comments</button>

    </div><!---End of feed-panel--->


    <div id="comments" class="collapse in">

        @foreach($tweet->comments as $comment)


        <div class="main-comment-container">

            <div class="feed-panel">
                <div class="row">
                    <div class="col-4">

                        <a href="/profile/{{ $comment->user->id }}"><strong class="text-success">{{ $comment->user->name }}</strong></a>

                    </div><!---End of col sm--->

                    <div class="col-8">

                        <small class="text-muted">{{ $comment->created_at->diffForHumans() }} </small>

                        <div class="dropdown float-right" >
                            <button type="button" class="btn btn-outline-light dropdown-toggle" data-toggle="dropdown">
                                <span class="dot"></span>
                                <span class="dot"></span>
                                <span class="dot"></span>
                            </button>


                            <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                <button class="dropdown-item" type="button">if auth Edit Comment</button>
                                <button class="dropdown-item" type="button">Embed Post</button>
                                <button class="dropdown-item" type="button">Report</button>
                            </div><!---End of dropdown-menu--->

                        </div><!---End of dropdown--->
                    </div><!---End of col sm--->

                </div><!---End of row--->
            </div><!---End of feed-panel--->

            <div class="comment-container">
                <div class="row">
                    <div class="col-4">

                        <a href="/profile/{{ $comment->user->id }}" class="pull-left">
                            <img src="/uploads/avatars/{{ $comment->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                        </a>

                    <div class="comment-likes">

                        Likes __

                    </div><!---End of likes--->
                </div><!---End of col-sm--->


                <div class="col-8">
                <div class="comment">

                    <blockquote class="blockquote mb-0">

                        @if($comment->gif_comment)
                        <img src="{{ $comment->body }}"  value="1"/>
                        @else
                        <p>
                            {{ $comment->body }}
                        </p>
                        @endif

                    </blockquote>

                </div><!---End of post--->
            </div><!---End of col-8--->
            </div><!---End of row--->
        </div><!---End of comment container--->


            <div class="feed-panel">
            </div><!---End of feed-panel--->

            <div class="write-comment">

                <div class="form-group">

                    <textarea class="form-control" rows="1" id="comment" placeholder="Reply to Comment..."></textarea>

                </div><!--End of form group--->

                <div class="form-group float-right">

                    <button class="btn btn-primary" type="submit">Reply to Comment</button>

                    <button class="btn btn-warning" @click.prevent="toggleBrowseGifs">
                        +Gif
                        <!-- <img src="/imgs/gif-icon.png" width="25px" /> -->
                    </button>

                </div><!---End of form group--->

            </div><!---End of comment box--->

            <div class="feed-panel">

                <button class="btn btn-outline-light" data-toggle="collapse" data-target=".replies-container">See Replies</button>

            </div><!---End of feed-panel--->

            @endforeach
        </div>
