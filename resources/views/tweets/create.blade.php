@include('layouts/nav')

<div class="container">

<br />
<h1>Create a Tweet</h1>
<br />
    <form action="/tweets" method="POST">
        {{csrf_field()}}


        <div class="form-group">
            <label>Body</label>
            <textarea class="form-control" name="body" placeholder="Write A Post"/></textarea>

        </div>

        <div class="form-group">

            <button class="btn btn-primary" type="submit">Save Tweet</button>

        </div>

    </form>

</div>
