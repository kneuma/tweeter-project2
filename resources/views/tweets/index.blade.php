
<div id="app">
    @foreach ($tweets as $tweet)
        <div class="panel panel-info">
            <div class="panel-heading">
                {{ $tweet->title }}

                 @if(Auth::id() == $tweet->user->id)
                <span class="text-muted pull-right">
                    <a href="/tweets/{{ $tweet->id }}/edit" class="btn btn-xs btn-primary">
                        Edit
                    </a>
                </span>
                @endif

            </div><!---End of panel-heading--->

            <div class="panel-body">
                <ul class="media-list">
                    <li class="media">
                        <a href="/profile/{{ $tweet->user->id }}" class="pull-left">
                            <img src="/uploads/avatars/{{ $tweet->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                        </a>
                        <div class="media-body">
                            <span class="text-muted pull-right">
                                <small class="text-muted">{{ $tweet->created_at }} </small>
                            </span>
                            <a href="/profile/{{ $tweet->user->id }}"><strong class="text-success">{{ $tweet->user->name }}</strong></a>
                            <p>
                                {{ $tweet->body }}
                            </p>
                            <like-button
                                like-count="{{ count($tweet->likes) }}"
                                has-liked="{{ $tweet->likedByCurrentUser() }}"
                                tweet-id="{{ $tweet->id }}">

                            </like-button>

                        </div><!---End of media-body--->
                    </li>
                </ul>

                <form method="POST" action ="/tweets/{{ $tweet->id }}/comments">
                    @csrf
                    <comment-form>   </comment-form>
                </form>


            </div><!---End of panel-body--->

            <div class="panel-group" id="accordion">

                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                            See Comments
                        </a>
                    </h4>
                </div><!---End of panel-heading--->

                <div id="collapse1" class="panel-collapse collapse in">

                    <div class="panel-body">
                    @foreach($tweet->comments as $comment)
                        <li class="media">
                            <a href="/profile/{{ $comment->user->id }}" class="pull-left">
                                <img src="/uploads/avatars/{{ $comment->user->avatar }}" style="width:75px; height=75px; border-radius:50%; margin-right:25px; float:left;">
                            </a>
                            <div class="media-body">
                                <span class="text-muted pull-right">
                                    <small class="text-muted">{{ $comment->created_at->diffForHumans() }} </small>
                                </span>

                                <a href="/profile/{{ $comment->user->id }}"><strong class="text-success">{{ $comment->user->name }}</strong></a>


                                @if($comment->gif_comment)
                                <img src="{{ $comment->body }}"  value="1"/>
                                @else
                                <p>
                                    {{ $comment->body }}
                                </p>
                                @endif

                                @if(Auth::id() == $comment->user->id)
                                <a href="/tweets/{{ $comment->id }}/comments/edit" class="btn btn-xs btn-primary">

                                        Edit
                                    </span>
                                </a>
                                @endif

                            </div>
                        </li>
                    @endforeach
                </div><!---End of panel-body--->
            </div><!---End of collapse1--->
        </div><!---End of panel-group--->
        @endforeach
    </div>
</div>
