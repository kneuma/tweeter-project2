<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>

        <title>Cluster Edit Profile</title>

        @include('layouts/head')

    </head>
<body>
    @include('layouts/nav')

    <div class="page-container2">
        <div class="container">

            <br />
            <h1>Edit Your Tweet</h1>

            <form action="/tweets/{{ $tweet->id}}" method="POST">
                <input type="hidden" name="_method" value="PUT"/>
                {{csrf_field()}}

                <div class="form-group">
                    <label>Body</label>
                    <textarea class="form-control" name="body" placeholder="Write A Post"/>{{ $tweet->body }}</textarea>

                </div>
                <br />
                <div class="form-group">

                    <button class="btn btn-primary" type="submit">Save Tweet</button>
                    <form action="/tweets/{{ $tweet->id }}" method="POST">

                    </form>

                </div>

                <br />
                <div class="form-group" >

                    <form action="/tweets/{{ $tweet->id }}" method ="POST">

                        <input type="hidden" name ="_method" value="DELETE" />
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-danger">Delete Post</button>

                    </form>
            </div>
        </div>
    </div>
    @include('layouts/script')
</body>
