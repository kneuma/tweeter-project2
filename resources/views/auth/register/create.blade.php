
<div class="home-text4">
<h1>Register</h1>

<form action="/auth/register" method="POST">
    @csrf
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Name</label>

                <input type="text" name="name" class="form-control" required/>

            </div><!---End of form-group--->
        </div><!---End of col--->

        <div class="col">
            <div class="form-group">
                <label>Username</label>

                <input type="text" name="username" class="form-control" required/>

            </div><!---End of form-group--->
        </div><!---End of col--->
    </div><!---End of Row--->

    <div class="form-group">
        <label>Email</label>

        <input type="email" name="email" class="form-control" required/>

    </div><!---End of form-group--->
    <div class="row">
        <div class="col">
            <div class="form-group">
                <label>Password</label>

                <input type="password" name="password" class="form-control" required/>

            </div><!---End of form-group--->
        </div><!---End of col--->
        <div class="col">
            <div class="form-group">
                <label>Confirm Password</label>

                <input type="password" name="password_confirmation" class="form-control" required/>

            </div><!---End of form-group--->
        </div><!---End of col--->
    </div><!---End of Row--->

    @if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
    @endif

    <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg">Register</button>

    </div>

</form>
</div>
