<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cluster Login/Register Now</title>

        <link href="/css/bootstrap.min.css" rel="stylesheet">
        <link href="/css/nav.css" rel="stylesheet">
        <link href="/css/mp.css" rel="stylesheet">


</head>

<body>

@include('layouts/nav')

<div class="parallax" data-parallax="scroll" data-z-index="-100" data-image-src="/imgs/milky-way.jpg">

    <div id="particles-js">

    </div><!---End of particles--->

</div><!---End of parallax--->

<div class="home-container">
    <div class="major-bg1">

                    <div class="home-text">

                        <h1>Login</h1>

                        <form action="/auth/login" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Email</label>

                                <input type="email" name="email" class="form-control"/>

                            </div><!---End of form-group--->


                            <div class="form-group">
                                <label>Password</label>

                                <input type="password" name="password" class="form-control"/>

                            </div><!---End of form-group--->

                            @if($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif

                            <div class="form-group">

                                <button type="submit" class="btn btn-primary btn-lg">Login</button>

                            </div><!---End of form-group--->

                        </form>

                    </div><!---End of home-text--->

        </div><!---End of major-bg1--->


    </div><!---End of home container--->

    <script src="/js/jquery.min.js"></script>
    <script src="/js/particles.js"></script>
    <script src="/js/particlesapp.js"></script>
    <script src="/js/parallax.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/jScrollability.js"></script>
    <script src="/js/scroll.js"></script>

    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
    </script>
</body>
</html>
