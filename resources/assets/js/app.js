
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
//save
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('commentForm', require('./components/CommentFormComponent.vue'));

Vue.component('gifSearch', require('./components/GifSearchComponent.vue'));

Vue.component('likeButton',require('./components/LikeButtonComponent.vue'))



window.onload = function () {
    var main = new Vue({
        el: '#app',
    });
}
