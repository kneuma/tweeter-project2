<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('home');
});
Route::get('feed', 'TweetController@index')->middleware('auth');


Route::get('feed', 'TweetController@feed');
Route::get('listusers', 'ProfileController@listusers');
Route::get('following', 'ProfileController@following');
Route::get('followers', 'ProfileController@followers');


Route::get('profile', 'ProfileController@show')->middleware('auth');
Route::get('profile/edit', 'ProfileController@edit');
Route::post('profile', 'ProfileController@update_avatar');
Route::put('profile/{profile}', 'ProfileController@update');
Route::get('profile/{profile}', 'ProfileController@show')->middleware('auth');

Route::get('auth/login', 'auth\LoginController@create')->name('login');
Route::post('auth/login', 'auth\LoginController@store');
Route::get('auth/logout', 'auth\LoginController@destroy')->name('logout');

Route::get('auth/register', 'auth\RegisterController@create');
Route::post('auth/register', 'auth\RegisterController@store');

Route::post('tweets/{tweet}/like', 'TweetController@like');
Route::post('tweets/{tweet}/unlike', 'TweetController@unlike');
Route::resource('tweets', 'TweetController')->middleware('auth');


Route::post('tweets/{tweet}/comments', 'CommentsController@store');
Route::get('tweets/{tweet}/comments/edit', 'CommentsController@edit');
Route::put('tweets/{tweet}/comments', 'CommentsController@update');
Route::delete('tweets/{tweet}/comments', 'CommentsController@destroy');

Route::get('profile/{profile_id}/follow', 'ProfileController@followUser')->name('user.follow');
Route::get('/{profile_id}/unfollow', 'ProfileController@unFollowUser')->name('user.unfollow');
